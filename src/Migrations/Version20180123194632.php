<?php declare(strict_types = 1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180123194632 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE usuario_compra_entrada DROP FOREIGN KEY FK_EVENTO_COMPRADO');
        $this->addSql('ALTER TABLE eventos DROP FOREIGN KEY FK_ID_GESTOR');
        $this->addSql('CREATE TABLE evento (id INT AUTO_INCREMENT NOT NULL, titulo VARCHAR(255) NOT NULL, imagen VARCHAR(255) NOT NULL, fecha_inicio_venta_entradas DATE NOT NULL, fecha_fin_venta_entradas DATE NOT NULL, fecha_celebracion DATE NOT NULL, hora TIME NOT NULL, cant_entradas_total INT NOT NULL, cant_entradas_restante INT NOT NULL, precio NUMERIC(9, 2) NOT NULL, descripcion LONGTEXT NOT NULL, ciudad LONGTEXT NOT NULL, lugar LONGTEXT NOT NULL, pagina_web LONGTEXT NOT NULL, id_gestor INT NOT NULL, email_gestor LONGTEXT NOT NULL, categoria LONGTEXT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('DROP TABLE eventos');
        $this->addSql('DROP TABLE usuario_compra_entrada');
        $this->addSql('DROP TABLE usuarios');
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE eventos (id INT AUTO_INCREMENT NOT NULL, id_gestor INT NOT NULL, titulo VARCHAR(255) NOT NULL COLLATE utf8_spanish2_ci, imagen VARCHAR(255) NOT NULL COLLATE utf8_spanish2_ci, fecha_inicio_venta_entradas DATE NOT NULL, fecha_fin_venta_entradas DATE NOT NULL, fecha_celebracion DATE NOT NULL, hora TIME NOT NULL, cant_entradas_total INT NOT NULL, cant_entradas_restante INT NOT NULL, precio NUMERIC(10, 0) NOT NULL, descripcion VARCHAR(255) NOT NULL COLLATE utf8_spanish2_ci, ciudad VARCHAR(255) NOT NULL COLLATE utf8_spanish2_ci, lugar VARCHAR(255) NOT NULL COLLATE utf8_spanish2_ci, pagina_web VARCHAR(255) NOT NULL COLLATE utf8_spanish2_ci, email_gestor VARCHAR(255) NOT NULL COLLATE utf8_spanish2_ci, categoria VARCHAR(255) NOT NULL COLLATE utf8_spanish2_ci, INDEX FK_ID_GESTOR (id_gestor), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE usuario_compra_entrada (id INT AUTO_INCREMENT NOT NULL, id_evento INT NOT NULL, usuario INT NOT NULL, titulo_evento VARCHAR(255) NOT NULL COLLATE utf8_spanish2_ci, fecha_evento DATE NOT NULL, cant_entradas INT NOT NULL, importe INT NOT NULL, INDEX FK_USUARIO_COMPRADOR (usuario), INDEX FK_EVENTO_COMPRADO (id_evento), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE usuarios (id INT AUTO_INCREMENT NOT NULL, nombre VARCHAR(255) NOT NULL COLLATE utf8_spanish2_ci, apellidos VARCHAR(255) NOT NULL COLLATE utf8_spanish2_ci, provincia VARCHAR(255) NOT NULL COLLATE utf8_spanish2_ci, correo VARCHAR(255) NOT NULL COLLATE utf8_spanish2_ci, password VARCHAR(255) NOT NULL COLLATE utf8_spanish2_ci, role VARCHAR(50) NOT NULL COLLATE utf8_spanish2_ci, salt VARCHAR(22) NOT NULL COLLATE utf8_spanish2_ci, avatar VARCHAR(255) DEFAULT NULL COLLATE utf8_spanish2_ci, tiene_eventos TINYINT(1) DEFAULT \'0\' NOT NULL, tiene_mensajes TINYINT(1) DEFAULT \'0\' NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE eventos ADD CONSTRAINT FK_ID_GESTOR FOREIGN KEY (id_gestor) REFERENCES usuarios (id) ON UPDATE CASCADE ON DELETE CASCADE');
        $this->addSql('ALTER TABLE usuario_compra_entrada ADD CONSTRAINT FK_EVENTO_COMPRADO FOREIGN KEY (id_evento) REFERENCES eventos (id) ON UPDATE CASCADE ON DELETE CASCADE');
        $this->addSql('DROP TABLE evento');
    }
}
