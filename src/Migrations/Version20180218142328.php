<?php declare(strict_types = 1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180218142328 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE usuario_compra_evento (id INT AUTO_INCREMENT NOT NULL, id_usuario_id INT DEFAULT NULL, id_evento_id INT DEFAULT NULL, cant_entradas INT NOT NULL, precio INT NOT NULL, fecha_compra DATE NOT NULL, INDEX IDX_9CFC3B9C7EB2C349 (id_usuario_id), INDEX IDX_9CFC3B9C7904465 (id_evento_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE usuario_compra_evento ADD CONSTRAINT FK_9CFC3B9C7EB2C349 FOREIGN KEY (id_usuario_id) REFERENCES usuario (id)');
        $this->addSql('ALTER TABLE usuario_compra_evento ADD CONSTRAINT FK_9CFC3B9C7904465 FOREIGN KEY (id_evento_id) REFERENCES evento (id)');
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE usuario_compra_evento');
    }
}
