<?php declare(strict_types = 1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180202103450 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE categoria (id INT AUTO_INCREMENT NOT NULL, nombre VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('DROP TABLE producto');
        $this->addSql('ALTER TABLE evento ADD categoria_id INT DEFAULT NULL, DROP categoria, CHANGE descripcion descripcion LONGTEXT DEFAULT NULL, CHANGE pagina_web pagina_web LONGTEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE evento ADD CONSTRAINT FK_47860B053397707A FOREIGN KEY (categoria_id) REFERENCES categoria (id)');
        $this->addSql('CREATE INDEX IDX_47860B053397707A ON evento (categoria_id)');
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE evento DROP FOREIGN KEY FK_47860B053397707A');
        $this->addSql('CREATE TABLE producto (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, descripcion LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci, precio NUMERIC(9, 2) NOT NULL, fecha_alta DATETIME NOT NULL, imagen VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('DROP TABLE categoria');
        $this->addSql('DROP INDEX IDX_47860B053397707A ON evento');
        $this->addSql('ALTER TABLE evento ADD categoria LONGTEXT NOT NULL COLLATE utf8_unicode_ci, DROP categoria_id, CHANGE descripcion descripcion LONGTEXT NOT NULL COLLATE utf8_unicode_ci, CHANGE pagina_web pagina_web LONGTEXT NOT NULL COLLATE utf8_unicode_ci');
    }
}
