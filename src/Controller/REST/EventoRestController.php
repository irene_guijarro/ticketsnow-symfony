<?php
/**
 * Created by PhpStorm.
 * User: irene
 * Date: 25/02/18
 * Time: 13:01
 */

namespace App\Controller\REST;


use App\BLL\EventoBLL;
use App\BLL\UsuarioBLL;
use App\Entity\Evento;
use App\Entity\Usuario;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class EventoRestController extends BaseApiController
{
    /**
     * @Route("/eventos.{_format}", name="get_eventos",
     * defaults={"_format": "json"},
     * requirements={"_format": "json"}
     * )
     * @Route("/eventos/ordenados/{order}", name="get_eventos_ordenados",
     *  requirements={"order": "titulo|precio|categoria|fechaCelebracion|id"}
     * )
     * @Method("GET")
     */
    public function getAll(Request $request, EventoBLL $eventoBLL, $order='titulo')
    {
        $busqueda = $request->query->get('busqueda');
        $busquedaCiudad = $request->query->get('busquedaCiudad');
        $busquedaCategoria = $request->query->get('categoria');
        $busquedaFecha = $request->query->get('fecha');

        $eventos = $eventoBLL->getEventosFiltrados($busqueda, $busquedaCiudad, $busquedaCategoria, $busquedaFecha, $order);

        return $this->getResponse($eventos);
    }

    /**
     * @Route("/eventos/{id}.{_format}", name="get_evento",
     * requirements={"id": "\d+", "_format": "json" },
     * defaults={"_format": "json"})
     * @Method("GET")
     */
    public function getOne(Evento $evento, EventoBLL $eventoBLL)
    {
        return $this->getResponse($eventoBLL->toArray($evento));
    }

    /**
     * @Route("/eventos.{_format}", name="post_eventos",
     *  defaults={"_format": "json"},
     *  requirements={"_format": "json"}
     * )
     * @Method("POST")
     */
    public function post(Request $request, EventoBLL $eventoBLL)
    {
        $data = $this->getContent($request);

        $evento = $eventoBLL->nuevo(
            $data['titulo'],
            $data['descripcion'],
            $data['precio'],
            $data['ciudad'],
            $data['lugar'],
            $data['categoria'],
            $data['fechaCelebracion'],
            $data['hora'],
            $data['fechaInicioVenta'],
            $data['fechaFinVenta'],
            $data['imagen'],
            $data['cantEntradas'],
            $data['paginaWeb']);

        return $this->getResponse($evento, Response::HTTP_CREATED );
    }

    /**
     * @Route("/eventos/{id}.{_format}", name="update_evento",
     *  requirements={"id": "\d+", "_format": "json" },
     *  defaults={"_format": "json"})
     * @Method("PUT")
     */
    public function update(Request $request, Evento $evento, EventoBLL $eventoBLL)
    {
        $data = $this->getContent($request);
        $evento = $eventoBLL->update($evento, $data);
        return $this->getResponse($evento, Response:: HTTP_OK );
    }

    /**
     * @Route("/eventos/{id}.{_format}", name="delete_evento",
     * requirements={ "id": "\d+", "_format": "json" },
     * defaults={"_format": "json"})
     * @Method("DELETE")
     */
    public function delete(Evento $evento, EventoBLL $eventoBLL)
    {
        $eventoBLL->delete($evento);
        return $this->getResponse(null, Response:: HTTP_NO_CONTENT );
    }

    /**
     * @Route("/eventos/{id}/imagen.{_format}", name="cambia_imagen_evento",
     * requirements={"id": "\d+", "_format": "json"},
     * defaults={"_format": "json"})
     * @Method("PATCH")
     */
    public function cambiaImagen(Request $request, EventoBLL $eventoBLL, Evento $evento)
    {
        $data = $this->getContent($request);
        if ( is_null ($data['imagen']))
            throw new BadRequestHttpException('No se ha recibido la imagen');

        $evento = $eventoBLL->cambiaImagen(
            $evento, $data['imagen']);

        return $this->getResponse($evento);
    }

}