<?php
/**
 * Created by PhpStorm.
 * User: irene
 * Date: 25/02/18
 * Time: 21:06
 */

namespace App\Controller\REST;

use App\BLL\UsuarioCompraEventoBLL;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class EntradaRestController extends BaseApiController
{
    /**
     * @Route("/entradas.{_format}", name="get_entradas",
     * defaults={"_format": "json"},
     * requirements={"_format": "json"}
     * )
     * @Method("GET")
     */
    public function getAll(UsuarioCompraEventoBLL $entradaBLL)
    {
        $entradas = $entradaBLL->getAll();

        return $this->getResponse($entradas);
    }
}