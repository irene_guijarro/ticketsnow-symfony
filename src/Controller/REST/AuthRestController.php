<?php
/**
 * Created by PhpStorm.
 * User: irene
 * Date: 25/02/18
 * Time: 12:59
 */

namespace App\Controller\REST;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AuthRestController
{
    /**
     * @Route("/auth/login")
     */
    public function getTokenAction()
    {
        return new Response('', Response:: HTTP_UNAUTHORIZED );
    }
}