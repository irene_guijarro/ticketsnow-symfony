<?php
/**
 * Created by PhpStorm.
 * User: irene
 * Date: 25/02/18
 * Time: 14:17
 */

namespace App\Controller\REST;

use App\BLL\UsuarioBLL;
use App\Entity\Usuario;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Routing\Annotation\Route;

class UsuarioRestController extends BaseApiController
{
    /**
     * @Route("/auth/register.{_format}", name="register",
     * requirements={"_format": "json"},
     * defaults={"_format": "json"})
     * @Method("POST")
     */
    public function register(Request $request, UsuarioBLL $usuarioBLL)
    {
        $data = $this->getContent($request);

        $user = $usuarioBLL->nuevo(
            $data['username'], $data['apellidos'], $data['provincia'], $data['password'],
            $data['email']);

        return $this->getResponse($user, Response:: HTTP_CREATED );
    }

    /**
     * @Route("/usuarios/{id}.{_format}", name="get_usuario",
     * requirements={"id": "\d+","_format": "json"},
     * defaults={"_format": "json"})
     * @Method("GET")
     */
    public function getOne(Usuario $user, UsuarioBLL $userBLL)
    {
        return $this->getResponse($userBLL->toArray($user));
    }

    /**
     * @Route("/usuarios.{_format}", name="get_usuarios",
     * defaults={"_format": "json"},
     * requirements={"_format": "json"}
     * )
     * @Method("GET")
     */
    public function getAll(UsuarioBLL $userBLL)
    {
        $users = $userBLL->getAll();

        return $this->getResponse($users);
    }

    /**
     * @Route("/usuarios/{id}.{_format}", name="delete_usuario",
     * requirements={ "id": "\d+", "_format": "json" },
     * defaults={"_format": "json"})
     * )
     * @Method("DELETE")
     */
    public function delete(Usuario $user, UsuarioBLL $userBLL)
    {
        $userBLL->delete($user);

        return $this->getResponse(null, Response::HTTP_NO_CONTENT);
    }

    /**
     * @Route("/usuarios/profile.{_format}", name="profile",
     * requirements={"_format": "json"},
     * defaults={"_format": "json"})
     * @Method("GET")
     */
    public function profile(UsuarioBLL $userBLL)
    {
        $user = $userBLL->profile();

        return $this->getResponse($user);
    }

    /**
     * @Route("/profile/avatar.{_format}", name="cambia_avatar",
     * requirements={
     *  "_format": "json"
     * },
     * defaults={"_format": "json"})
     * @Method("PATCH")
     */
    public function cambiaAvatar(Request $request, UsuarioBLL $userBLL)
    {
        $data = $this->getContent($request);
        if ( is_null ($data['imagen']))
            throw new BadRequestHttpException('No se ha recibido la imagen');

        $usuario = $userBLL->cambiaImagen($data['imagen']);

        return $this->getResponse($usuario);
    }

    /**
     * @Route("/profile/password.{_format}", name="cambia_password",
     * requirements={ "_format": "json" },
     * defaults={"_format": "json"})
     * @Method("PATCH")
     */
    public function cambiaPassword(Request $request, UsuarioBLL $userBLL)
    {
        $data = $this->getContent($request);
        if ( is_null ($data['password']) || !isset($data['password']) || empty($data['password']))
            throw new BadRequestHttpException('No se ha recibido el password');

        $user = $userBLL->cambiaPassword($data['password']);

        return $this->getResponse($user);
    }


}