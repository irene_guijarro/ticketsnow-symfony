<?php
/**
 * Created by PhpStorm.
 * User: irene
 * Date: 23/01/18
 * Time: 20:15
 */

namespace App\Controller;


use App\BLL\EventoBLL;
use App\BLL\UsuarioBLL;
use App\BLL\UsuarioCompraEventoBLL;
use App\Entity\Evento;
use App\Entity\Usuario;
use App\Entity\Usuario_Compra_Evento;
use App\Form\EventoType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints\Date;
use Symfony\Component\Validator\Constraints\DateTime;

/**
 * @Route("/eventos")
 */

class EventoController extends Controller
{
    private function getEventos (EventoBLL $eventoBll, string $busqueda = null, string $page = null, string $busquedaCiudad = null,
                                 string $busquedaFecha = null, string $busquedaCategoria = null)
    {
        $numEventosPagina = 3;
        if (is_null($page))
        {
            $offset = null;
            $page = 1;
        }
        else
            $offset = ($page - 1) * $numEventosPagina;

        $eventos = $eventoBll->getEventos($busqueda, $busquedaCiudad, $busquedaFecha,
            $offset, $numEventosPagina, $busquedaCategoria);

        if (!is_null($busqueda) || !is_null($busquedaCiudad) || !is_null($busquedaCategoria) || !is_null($busquedaFecha))
        {
            $numEventosFiltrados = $eventoBll->getNumEventos($busqueda, $busquedaCiudad, $busquedaCategoria, $busquedaFecha);
            $numEventos = $eventoBll->getNumEventos();
        }
        else
        {
            $numEventos = $eventoBll->getNumEventos();
            $numEventosFiltrados = $numEventos;
        }

        return [
            'eventos' => $eventos,
            'numEventos' => $numEventos,
            'numEventosPagina' => $numEventosPagina,
            'numEventosFiltrados' => $numEventosFiltrados,
            'busqueda' => $busqueda,
            'busquedaCiudad' => $busquedaCiudad,
            'busquedaFecha' => $busquedaFecha,
            'busquedaCategoria' => $busquedaCategoria,
            'page' => $page
        ];
    }

    private function getEvento(EventoBLL $eventoBLL, int $id)
    {
        $evento = $eventoBLL->getEvento($id);

        return [
            'evento' => $evento
        ];
    }

    private function getEventosGestor (EventoBLL $eventoBll, int $idUsuario)
    {
        $eventos = $eventoBll->getEventos($idUsuario);

        $numEventos = $eventoBll->getNumEventos();

        return [
            'eventos' => $eventos,
            'numEventos' => $numEventos
        ];
    }

    /**
     * @Route("/", name="ticketsnow_eventos")
     * @Template("eventos/eventos.html.twig")
     */
    public function listar(Request $request, EventoBLL $eventoBLL)
    {
        $page = $request->query->get('page');
        $busqueda = $request->query->get('busqueda');

        return $this->getEventos($eventoBLL, $busqueda, $page);
    }

    /**
     * @Route("/{id}", name="ticketsnow_eventos_detalle",
     *     requirements={"id": "\d+"})
     * @Method("GET")
     * @Template("eventos/detalles.html.twig")
     */
    public function detalle(EventoBLL $eventoBLL, $id)
    {
        return $this->getEvento($eventoBLL, $id);
    }

    private function formEvento(Request $request, EventoBLL $eventoBLL, Evento $evento, UsuarioBLL $usuarioBLL, Usuario $gestor)
    {
        $form = $this->createForm(EventoType::class, $evento);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid())
        {
            $evento->setIdGestor($gestor);
            $evento->setCantEntradasRestante($evento->getCantEntradasTotal());
            $gestor->setTieneEventos(true);
            $usuarioBLL->guardaUsuario($gestor);
            $eventoBLL->guardaEvento($evento);

            return $this->redirectToRoute('ticketsnow_eventos');
        }

        return $this->render(
            'eventos/form-evento.html.twig',
            [
                'form' => $form->createView()
            ]
        );

    }

    /**
     * @Route("/nuevo", name="ticketsnow_eventos_nuevo")
     * @Method({"GET", "POST"})
     */
    public function nuevo(Request $request, EventoBLL $eventoBLL, UsuarioBLL $usuarioBLL)
    {
        $evento = new Evento();
        $gestor = $this->getUser();

        return $this->formEvento($request, $eventoBLL, $evento, $usuarioBLL, $gestor);
    }

    /**
     * @Route("/{id}/eliminar", name="ticketsnow_eventos_eliminar")
     * @Method("GET")
     */
    public function eliminar(EventoBLL $eventoBLL, $id)
    {
        $eventoBLL->eliminaEvento($id);

        return $this->redirectToRoute('ticketsnow_eventos_gestor');
    }

    /**
     * @Route("/buscar", name="ticketsnow_eventos_buscar")
     * @Method("POST")
     * @Template("eventos/eventos.html.twig")
     */
    public function buscar(Request $request, EventoBLL $eventoBLL)
    {
        $page = $request->query->get('page');

        $busqueda = $request->request->get('busqueda');
        $busquedaCiudad = $request->request->get('busquedaCiudad');
        $busquedaCategoria = $request->request->get('categoria');
        $busquedaFecha = $request->request->get('fecha');

        return $this->getEventos($eventoBLL, $busqueda, $page, $busquedaCiudad, $busquedaFecha, $busquedaCategoria);
    }

    /**
     * @Route("/{id}/editar", name="ticketsnow_eventos_editar")
     * @Method({"GET", "POST"})
     */
    public function editar(
        Request $request, EventoBLL $eventoBLL, Evento $evento, UsuarioBLL $usuarioBLL)
    {
        $gestor = $this->getUser();
        $evento->setImagen(
            new File(
                $this->getParameter('events_directory') . '/' . $evento->getImagen()
            )
        );
        return $this->formEvento($request, $eventoBLL, $evento, $usuarioBLL, $gestor);
    }

    /**
     * @Route("/gestor", name="ticketsnow_eventos_gestor")
     * @Template("eventos/mis-eventos.html.twig")
     */
    public function eventosGestor(EventoBLL $eventoBLL)
    {
        $usuario = $this->getUser();

        if ($usuario->getRole() === 'ROLE_ADMIN')
        {
            $eventos = $eventoBLL->getAll();

        }
        else
        {
            $eventos = $eventoBLL->getEventosGestor($usuario->getId());
        }

        return [
            'eventos' => $eventos
        ];
    }

    /**
     * @Route("/{id}/comprar", name="ticketsnow_eventos_comprar")
     * @Method({"GET", "POST"})
     * @Template("eventos/factura.html.twig")
     */
    public function comprar(Request $request, EventoBLL $eventoBLL, Evento $evento, UsuarioCompraEventoBLL $usuarioCompraEventoBLL)
    {
        $usuario = $this->getUser();
        $fecha = new \DateTime(date('Y-m-d H:i:s'));

        if ($evento->getFechaCelebracion() < $fecha)
        {
            return $this->render(
                'eventos/errorCompra.html.twig',
                [
                    'evento' => $evento,
                    'usuario' => $usuario
                ]
            );
        }

        $cantEntradas = $request->get('numTickets');
        $precioTotal = $evento->getPrecio() * $cantEntradas;

        $evento->setCantEntradasRestante($evento->getCantEntradasRestante() - $cantEntradas);
        $eventoBLL->guardaEvento($evento);

        $entrada = $this->creaEntrada($precioTotal, $cantEntradas, $fecha, $evento, $usuario);
        $usuarioCompraEventoBLL->guardaEntrada($entrada);

        return [
            'entrada' => $entrada,
            'evento' => $evento,
            'usuario' => $usuario
        ];
    }

    private function creaEntrada($precioTotal, $cantEntradas, $fecha, $evento, $usuario)
    {
        $entrada = new Usuario_Compra_Evento();
        $entrada->setPrecio($precioTotal);
        $entrada->setCantEntradas($cantEntradas);
        $entrada->setFechaCompra($fecha);
        $entrada->setIdEvento($evento);
        $entrada->setIdUsuario($usuario);

        return $entrada;
    }
}