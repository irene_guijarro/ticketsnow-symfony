<?php
/**
 * Created by PhpStorm.
 * User: irene
 * Date: 9/02/18
 * Time: 17:55
 */

namespace App\Controller;


use App\BLL\UsuarioBLL;
use App\BLL\UsuarioCompraEventoBLL;
use App\Entity\Usuario;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/usuarios")
 */
class UsuarioController extends Controller
{
    /**
     * @Route("/gestion", name="ticketsnow_usuarios_gestion")
     * @Template("usuarios/gestion-usuarios.html.twig")
     */
    public function listar(UsuarioBLL $usuarioBLL)
    {
        $usuarios = $usuarioBLL->getAllUsuarios();

        return [
            'usuarios' => $usuarios
        ];
    }

    /**
     * @Route("/{id}/cambiarRole", name="cambiarRole")
     * @Method("POST")
     */
    public function cambiarRole(Request $request, UsuarioBLL $usuarioBLL, $id)
    {
        $nuevoRole = $request->get('role');

        $usuario = $usuarioBLL->getUsuario($id);
        $usuario->setRole($nuevoRole);

        $usuarioBLL->guardaUsuario($usuario);

        return $this->redirectToRoute('ticketsnow_usuarios_gestion');
    }

    /**
     * @Route("/{id}/eliminar", name="ticketsnow_usuarios_eliminar")
     * @Method("GET")
     */
    public function eliminar(UsuarioBLL $usuarioBLL, $id)
    {
        $usuario = $usuarioBLL->getUsuario($id);
        $usuarioBLL->delete($usuario);

        return $this->redirectToRoute('ticketsnow_usuarios_gestion');
    }

    /**
     * @Route("/entradas", name="ticketsnow_usuarios_entradas")
     * @Method("GET")
     * @Template("usuarios/mis-entradas.html.twig")
     */
    public function listarEntradas(UsuarioCompraEventoBLL $entradasBLL)
    {
        $entradas = $entradasBLL->entradasUsuario();

        return [
            'entradas' => $entradas
        ];
    }

    /**
     * @Route("/profile", name="ticketsnow_profile")
     * @Method({"GET", "POST"})
     * @Template("usuarios/perfil.html.twig")
     */
    public function profile(UsuarioBLL $usuarioBLL)
    {
        $usuario = $usuarioBLL->profile();

        return [
            'usuario' => $usuario
        ];
    }

    /**
     * @Route("/password", name="cambiarPassword")
     * @Method("POST")
     */
    public function cambiarPassword(Request $request, UsuarioBLL $usuarioBLL)
    {
        $nuevaPassword = $request->get('password');

        $usuarioBLL->cambiaPassword($nuevaPassword);

        return $this->redirectToRoute('ticketsnow_profile');
    }
}