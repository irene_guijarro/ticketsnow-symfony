<?php

namespace App\Controller;

use App\BLL\UsuarioBLL;
use App\Entity\Usuario;
use App\Form\UsuarioType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;


class SecurityController extends Controller
{
    /**
     * @Route("/login", name="login")
     * @Template("security/login.html.twig")
     */
    public function login(Request $request, AuthenticationUtils $authUtils)
    {
        // get the login error if there is one
        $error = $authUtils->getLastAuthenticationError();
        // last username entered by the user
        $lastUsername = $authUtils->getLastUsername();

        return array(
            'last_username' => $lastUsername,
            'error' => $error,
        );
    }

    /**
     * @Route("/registrar", name="ticketsnow_usuario_registrar")
     * @Method({"GET", "POST"})
     */
    public function registrar(Request $request, UsuarioBLL $usuarioBLL, UserPasswordEncoderInterface $encoder)
    {
        $usuario = new Usuario();
        $usuario->setRole('ROLE_GESTOR');

        return $this->formUsuario($request, $usuarioBLL, $usuario, $encoder);
    }

    /**
     * @Route("/registrar/comprador", name="ticketsnow_usuario_registrarComprador")
     * @Method({"GET", "POST"})
     */
    public function registrarComprador(Request $request, UsuarioBLL $usuarioBLL, UserPasswordEncoderInterface $encoder)
    {
        $usuario = new Usuario();
        $usuario->setRole('ROLE_COMPRADOR');

        return $this->formUsuario($request, $usuarioBLL, $usuario, $encoder);
    }

    private function formUsuario(Request $request, UsuarioBLL $usuarioBLL, Usuario $usuario, UserPasswordEncoderInterface $encoder)
    {
        $form = $this->createForm(UsuarioType::class, $usuario);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid())
        {
            //TODO comprobar que contraseñas coincidan
            $encoded = $encoder->encodePassword($usuario, $usuario->getPassword());
            $usuario->setPassword($encoded);
            $usuarioBLL->guardaUsuario($usuario);

            return $this->redirectToRoute('ticketsnow_eventos');
        }

        return $this->render(
            'usuarios/crear.html.twig',
            [
                'form' => $form->createView()
            ]
        );
    }

    /**
     * @Route("/logout", name="logout")
     * @Template("security/login.html.twig")
     */
    public function logout(Request $request, AuthenticationUtils $authUtils)
    {
        // get the login error if there is one
        $error = $authUtils->getLastAuthenticationError();
        // last username entered by the user
        $lastUsername = $authUtils->getLastUsername();

        return array(
            'last_username' => $lastUsername,
            'error' => $error,
        );
    }
}