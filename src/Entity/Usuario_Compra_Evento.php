<?php
/**
 * Created by PhpStorm.
 * User: irene
 * Date: 2/02/18
 * Time: 10:59
 */

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UsuarioCompraEventoRepository")
 */
class Usuario_Compra_Evento
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Usuario")
     */
    private $idUsuario;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Evento")
     */
    private $idEvento;

    /**
     * @ORM\Column(type="integer")
     */
    private $cantEntradas;

    /**
     * @ORM\Column(type="integer")
     */
    private $precio;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getIdUsuario()
    {
        return $this->idUsuario;
    }

    /**
     * @param mixed $idUsuario
     */
    public function setIdUsuario($idUsuario)
    {
        $this->idUsuario = $idUsuario;
    }

    /**
     * @return mixed
     */
    public function getIdEvento()
    {
        return $this->idEvento;
    }

    /**
     * @param mixed $idEvento
     */
    public function setIdEvento($idEvento)
    {
        $this->idEvento = $idEvento;
    }

    /**
     * @return mixed
     */
    public function getCantEntradas()
    {
        return $this->cantEntradas;
    }

    /**
     * @param mixed $cantEntradas
     */
    public function setCantEntradas($cantEntradas)
    {
        $this->cantEntradas = $cantEntradas;
    }

    /**
     * @return mixed
     */
    public function getPrecio()
    {
        return $this->precio;
    }

    /**
     * @param mixed $precio
     */
    public function setPrecio($precio)
    {
        $this->precio = $precio;
    }

    /**
     * @return mixed
     */
    public function getFechaCompra()
    {
        return $this->fechaCompra;
    }

    /**
     * @param mixed $fechaCompra
     */
    public function setFechaCompra($fechaCompra)
    {
        $this->fechaCompra = $fechaCompra;
    }

    /**
     * @ORM\Column(type="date")
     */
    private $fechaCompra;
}