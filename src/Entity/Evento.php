<?php
/**
 * Created by PhpStorm.
 * User: irene
 * Date: 23/01/18
 * Time: 20:22
 */

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\EventoRepository")
 */

class Evento
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     * @Assert\NotBlank(message="No puedes dejar el campo título vacío")
     */
    private $titulo;

    /**
     * @ORM\Column(type="string")
     */
    private $imagen;

    /**
     * @ORM\Column(type="datetime")
     * @Assert\NotBlank(message="No puedes dejar el campo de fecha de inicio de venta vacío")
     */
    private $fecha_inicio_venta_entradas;

    /**
     * @ORM\Column(type="datetime")
     * @Assert\NotBlank(message="No puedes dejar el campo de fecha de fin de venta vacío")
     */
    private $fecha_fin_venta_entradas;

    /**
     * @ORM\Column(type="datetime")
     * @Assert\NotBlank(message="No puedes dejar el campo de fecha de celebración vacío")
     */
    private $fechaCelebracion;

    /**
     * @ORM\Column(type="time")
     * @Assert\NotBlank(message="No puedes dejar el campo de hora vacío")
     */
    private $hora;

    /**
     * @ORM\Column(type="integer")
     * @Assert\NotBlank(message="No puedes dejar el campo de cantidad de entradas vacío")
     */
    private $cant_entradas_total;

    /**
     * @ORM\Column(type="integer")
     */
    private $cant_entradas_restante;

    /**
     * @ORM\Column(type="decimal", precision=9, scale=2)
     * @Assert\NotBlank(message="No puedes dejar el campo de precio vacío")
     */
    private $precio;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $descripcion;

    /**
     * @ORM\Column(type="text")
     * @Assert\NotBlank(message="No puedes dejar el campo ciudad vacío")
     */
    private $ciudad;

    /**
     * @ORM\Column(type="text")
     * @Assert\NotBlank(message="No puedes dejar el campo lugar vacío")
     */
    private $lugar;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $pagina_web;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Usuario", inversedBy="App\Entity\Evento")
     * @ORM\JoinColumn(name="usuario_id", referencedColumnName="id")
     */
    private $id_gestor;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Categoria")
     */
    private $categoria;

    /**
     * Evento constructor.
     */
    public function __construct()
    {
        $this->cant_entradas_restante = $this->cant_entradas_total;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getTitulo()
    {
        return $this->titulo;
    }

    /**
     * @param mixed $titulo
     */
    public function setTitulo($titulo)
    {
        $this->titulo = $titulo;
    }

    /**
     * @return mixed
     */
    public function getImagen()
    {
        return $this->imagen;
    }

    /**
     * @param mixed $imagen
     */
    public function setImagen($imagen)
    {
        $this->imagen = $imagen;
    }

    /**
     * @return mixed
     */
    public function getFechaInicioVentaEntradas()
    {
        return $this->fecha_inicio_venta_entradas;
    }

    /**
     * @param mixed $fecha_inicio_venta_entradas
     */
    public function setFechaInicioVentaEntradas($fecha_inicio_venta_entradas)
    {
        $this->fecha_inicio_venta_entradas = $fecha_inicio_venta_entradas;
    }

    /**
     * @return mixed
     */
    public function getFechaFinVentaEntradas()
    {
        return $this->fecha_fin_venta_entradas;
    }

    /**
     * @param mixed $fecha_fin_venta_entradas
     */
    public function setFechaFinVentaEntradas($fecha_fin_venta_entradas)
    {
        $this->fecha_fin_venta_entradas = $fecha_fin_venta_entradas;
    }

    /**
     * @return mixed
     */
    public function getFechaCelebracion()
    {
        return $this->fechaCelebracion;
    }

    /**
     * @param mixed $fecha_celebracion
     */
    public function setFechaCelebracion($fechaCelebracion)
    {
        $this->fechaCelebracion = $fechaCelebracion;
    }

    /**
     * @return mixed
     */
    public function getHora()
    {
        return $this->hora;
    }

    /**
     * @param mixed $hora
     */
    public function setHora($hora)
    {
        $this->hora = $hora;
    }

    /**
     * @return mixed
     */
    public function getCantEntradasTotal()
    {
        return $this->cant_entradas_total;
    }

    /**
     * @param mixed $cant_entradas_total
     */
    public function setCantEntradasTotal($cant_entradas_total)
    {
        $this->cant_entradas_total = $cant_entradas_total;
    }

    /**
     * @return mixed
     */
    public function getCantEntradasRestante()
    {
        return $this->cant_entradas_restante;
    }

    /**
     * @param mixed $cant_entradas_restante
     */
    public function setCantEntradasRestante($cant_entradas_restante)
    {
        $this->cant_entradas_restante = $cant_entradas_restante;
    }

    /**
     * @return mixed
     */
    public function getPrecio()
    {
        return $this->precio;
    }

    /**
     * @param mixed $precio
     */
    public function setPrecio($precio)
    {
        $this->precio = $precio;
    }

    /**
     * @return mixed
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * @param mixed $descripcion
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;
    }

    /**
     * @return mixed
     */
    public function getCiudad()
    {
        return $this->ciudad;
    }

    /**
     * @param mixed $ciudad
     */
    public function setCiudad($ciudad)
    {
        $this->ciudad = $ciudad;
    }

    /**
     * @return mixed
     */
    public function getLugar()
    {
        return $this->lugar;
    }

    /**
     * @param mixed $lugar
     */
    public function setLugar($lugar)
    {
        $this->lugar = $lugar;
    }

    /**
     * @return mixed
     */
    public function getPaginaWeb()
    {
        return $this->pagina_web;
    }

    /**
     * @param mixed $pagina_web
     */
    public function setPaginaWeb($pagina_web)
    {
        $this->pagina_web = $pagina_web;
    }

    /**
     * @return mixed
     */
    public function getIdGestor()
    {
        return $this->id_gestor;
    }

    /**
     * @param mixed $id_gestor
     */
    public function setIdGestor($id_gestor)
    {
        $this->id_gestor = $id_gestor;
    }


    /**
     * @return mixed
     */
    public function getCategoria()
    {
        return $this->categoria;
    }

    /**
     * @param mixed $categoria
     */
    public function setCategoria($categoria)
    {
        $this->categoria = $categoria;
    }
}