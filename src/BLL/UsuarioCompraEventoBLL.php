<?php
/**
 * Created by PhpStorm.
 * User: irene
 * Date: 22/02/18
 * Time: 12:10
 */

namespace App\BLL;


use App\Entity\Usuario_Compra_Evento;
use Doctrine\Common\Persistence\ObjectManager;

class UsuarioCompraEventoBLL extends BaseBLL
{
    public function guardaEntrada(Usuario_Compra_Evento $entrada)
    {
        $this->em->persist($entrada);
        $this->em->flush();
    }

    public function getAll()
    {
        $eventos = $this->em->getRepository(Usuario_Compra_Evento::class)->findAll();
        return $this->entitiesToArray($eventos);
    }

    public function entradasUsuario()
    {
        $user = $this->getUser();
        return $this->em->getRepository(Usuario_Compra_Evento::class)->findEntradasUsuario($user->getId());
    }

    public function toArray($entrada)
    {
        if(is_null($entrada))
            return null;

        if(!($entrada instanceof Usuario_Compra_Evento))
            throw new \Exception("La entidad no es una Entrada");

        return [
            'id' => $entrada->getId(),
            'usuario' => $entrada->getIdUsuario(),
            'evento' => $entrada->getIdEvento(),
            'cantEntradas' => $entrada->getCantEntradas(),
            'precio' => $entrada->getPrecio(),
            'fechaCompra' => $entrada->getFechaCompra()
        ];
    }
}