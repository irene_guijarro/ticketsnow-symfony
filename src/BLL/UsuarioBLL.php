<?php
/**
 * Created by PhpStorm.
 * User: irene
 * Date: 9/02/18
 * Time: 18:45
 */

namespace App\BLL;


use App\Entity\Usuario;
use App\Service\FileUploader;
use Doctrine\Common\Persistence\ObjectManager;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UsuarioBLL extends BaseBLL
{
    /**
     * @var FileUploader $uploader
     */
    private $uploader;

    public function setUploader(FileUploader $uploader)
    {
        $this->uploader = $uploader;
    }

    /** @var UserPasswordEncoderInterface $encoder
     */
    private $encoder;
    public function setEncoder(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    /**
     * @var JWTTokenManagerInterface
     */
    private $jwtManager;

    public function setJWTManager(JWTTokenManagerInterface $jwtManager)
    {
        $this->jwtManager = $jwtManager;
    }

    public function nuevo($username, $apellidos, $provincia, $password, $email)
    {
        $user = new Usuario();

        $user->setUsername($username);
        $user->setPassword($this->encoder->encodePassword($user, $password));
        $user->setEmail($email);
        $user->setApellidos($apellidos);
        $user->setAvatar('');
        $user->setProvincia($provincia);
        $user->setRole('ROLE_GESTOR');
        $user->setTieneEventos(false);
        $user->setTieneMensajes(false);

        return $this->guardaValidando($user);
    }

    public function getAll()
    {
        $users = $this->em->getRepository(Usuario::class)->findAll();

        return $this->entitiesToArray($users);
    }

    public function getAllUsuarios()
    {
        $users = $this->em->getRepository(Usuario::class)->findAll();

        return $users;
    }

    public function getUsuario($id)
    {
        $user = $this->em->getRepository(Usuario::class)->findOneBy(['id' => $id]);

        return $user;
    }

    public function profile()
    {
        $user = $this->getUser();

        return $this->toArray($user);
    }

    public function getNumUsuarios()
    {
        return $this->em->getRepository(Usuario::class)->getCountUsuarios();
    }

    public function delete($usuario)
    {
        $this->em->remove($usuario);
        $this->em->flush();
    }

    public function guardaUsuario(Usuario $usuario)
    {
        $this->em->persist($usuario);
        $this->em->flush();
    }

    public function cambiaPassword($nuevoPassword)
    {
        $user = $this->getUser();

        $user->setPassword($this->encoder->encodePassword($user, $nuevoPassword));

        return $this->guardaValidando($user);
    }

    public function getTokenByEmail($email)
    {
        $user = $this->em->getRepository(Usuario:: class )
            ->findOneBy(array('email'=>$email));

        if ( is_null ($user))
            throw new AccessDeniedHttpException('Usuario no autorizado');

        return $this->jwtManager->create($user);
    }

    public function cambiaImagen($imagenB64)
    {
        $usuario = $this->getUser();

        $fileName = $this->uploader->saveB64Imagen($imagenB64);

        $usuario->setAvatar($fileName);

        return $this->guardaValidando($usuario);
    }

    public function toArray($usuario)
    {
        if(is_null($usuario))
            return null;

        if(!($usuario instanceof Usuario))
            throw new \Exception("La entidad no es un Usuario");

        return [
            'id' => $usuario->getId(),
            'username' => $usuario->getUsername(),
            'apellidos' => $usuario->getApellidos(),
            'avatar' => $usuario->getAvatar(),
            'email' => $usuario->getEmail(),
            'provincia' => $usuario->getProvincia(),
            'role' => $usuario->getRole()
        ];
    }
}