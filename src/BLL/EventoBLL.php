<?php
/**
 * Created by PhpStorm.
 * User: irene
 * Date: 29/01/18
 * Time: 16:44
 */
namespace App\BLL;


use App\Entity\Categoria;
use App\Entity\Evento;
use App\Entity\Usuario;
use App\Service\FileUploader;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Validator\Constraints\DateTime;

class EventoBLL extends BaseBLL
{
    /**
     * @var FileUploader $uploader
     */
    private $uploader;

    public function setUploader(FileUploader $uploader)
    {
        $this->uploader = $uploader;
    }

    public function getAll()
    {
        return $this->em->getRepository(Evento::class)->findAll();
    }

    public function getEventos(string $busqueda = null, string $busquedaCiudad = null, $busquedaFecha = null,
                               $offset = null, $limit = null, string $busquedaCatgoria = null, int $idUsuario = null)
    {
        return $this->em->getRepository(Evento::class)->findEventos($idUsuario, $busqueda, $busquedaCiudad,
            $busquedaFecha, $busquedaCatgoria, $offset, $limit);
    }

    // API
    public function getEventosFiltrados($busqueda, $busquedaCiudad, $busquedaCategoria, $busquedaFecha, $order)
    {
        $eventos = $this->em->getRepository(Evento::class)
            ->getEventosFiltrados($busqueda, $busquedaCiudad, $busquedaCategoria, $busquedaFecha, $order);
        return $this->entitiesToArray($eventos);
    }

    public function getEvento($id)
    {
        return $this->em->getRepository(Evento::class)->find($id);
    }

    public function getEventosGestor(int $idUsuario = null)
    {
        return $this->em->getRepository(Evento::class)->findEventos($idUsuario);
    }


    public function getNumEventos(string $busqueda = null, string $busquedaCiudad = null, $busquedacategoria = null,
                                  $busquedaFecha = null, $idUsuario = null)
    {
        return $this->em->getRepository(Evento::class)->getCountEventos($busqueda, $busquedaCiudad,
            $busquedacategoria, $busquedaFecha, $idUsuario);
    }

    public function nuevo($titulo, $descripcion, $precio, $ciudad, $lugar, $tipo, $fechaCelebracion, $hora, $fechaInicioVenta,
                            $fechaFinVenta, $imagen, $cantEntradas, $paginaWeb = null)
    {
        $fechaCelebracion = new \DateTime($fechaCelebracion);
        $fechaFinVenta = new \DateTime($fechaFinVenta);
        $fechaInicioVenta = new \DateTime($fechaInicioVenta);

        $hora = new \DateTime($hora);

        $evento = new Evento();
        $evento->setTitulo($titulo);
        $evento->setDescripcion($descripcion);
        $evento->setPrecio($precio);
        $evento->setCiudad($ciudad);
        $evento->setLugar($lugar);
        $evento->setFechaCelebracion($fechaCelebracion);
        $evento->setHora($hora);
        $evento->setFechaInicioVentaEntradas($fechaInicioVenta);
        $evento->setFechaFinVentaEntradas($fechaFinVenta);

        $fileName = $this->uploader->saveB64Imagen($imagen);

        $evento->setImagen($fileName);

        $evento->setCantEntradasTotal($cantEntradas);
        $evento->setCantEntradasRestante($cantEntradas);
        $evento->setPaginaWeb($paginaWeb);
        $evento->setIdGestor($this->getUser());

        $categoria = $this->em->getRepository(Categoria::class)
            ->findOneBy(['nombre' => $tipo]);

        $evento->setCategoria($categoria);


        return $this->guardaValidando($evento);
    }

    public function update(Evento $evento, array $data)
    {
        $fechaCelebracion = new \DateTime($data['fechaCelebracion']);
        $fechaInicioVenta = new \DateTime($data['fechaInicioVenta']);
        $fechaFinVenta = new \DateTime($data['fechaFinVenta']);

        $hora = new \DateTime($data['hora']);

        $categoria = $this->em->getRepository(Categoria::class)
            ->findOneBy(['nombre' => $data['categoria']]);

        $evento->setTitulo($data['titulo']);
        $evento->setDescripcion($data['descripcion']);
        $evento->setPrecio($data['precio']);
        $evento->setCiudad($data['ciudad']);
        $evento->setLugar($data['lugar']);
        $evento->setFechaCelebracion($fechaCelebracion);
        $evento->setHora($hora);
        $evento->setFechaInicioVentaEntradas($fechaInicioVenta);
        $evento->setFechaFinVentaEntradas($fechaFinVenta);
        $evento->setCantEntradasTotal($data['cantEntradas']);
        $evento->setPaginaWeb($data['paginaWeb']);
        $evento->setCategoria($categoria);

        return $this->guardaValidando($evento);
    }

    public function eliminaEvento($id)
    {
        $evento = $this->em->getRepository(Evento::class)->find($id);

        $this->em->remove($evento);
        $this->em->flush();
    }

    public function guardaEvento(Evento $evento)
    {
        $this->em->persist($evento);
        $this->em->flush();
    }

    public function cambiaImagen(Evento $evento, $imagenB64)
    {
        $fileName = $this->uploader->saveB64Imagen($imagenB64);

        $evento->setImagen($fileName);

        return $this->guardaValidando($evento);
    }

    public function toArray($evento)
    {
        if ( is_null ($evento))
            return null;

        if (!($evento instanceof Evento))
            throw new \Exception("La entidad no es un Evento");

        return [
            'id' => $evento->getId(),
            'titulo' => $evento->getTitulo(),
            'descripcion' => $evento->getDescripcion(),
            'precio' => $evento->getPrecio(),
            'ciudad' => $evento->getCiudad(),
            'lugar' => $evento->getLugar(),
            'categoria' => $evento->getCategoria()->getNombre(),
            'fechaCelebracion' => $evento->getFechaCelebracion()->format("d-m-Y"),
            'hora' => $evento->getHora()->format("H:i:s"),
            'fechaInicioVenta' => $evento->getFechaInicioVentaEntradas()->format("d-m-Y"),
            'fechaFinVenta' => $evento->getFechaFinVentaEntradas()->format("d-m-Y"),
            'imagen' => $evento->getImagen(),
            'cantEntradas' => $evento->getCantEntradasTotal(),
            'entradasRestantes' => $evento->getCantEntradasRestante(),
            'paginaWeb' => $evento->getPaginaWeb(),
            'gestor' => $evento->getIdGestor()

//            'usuario' => $producto->getUsuario()->getUsername(),
        ];
    }
}