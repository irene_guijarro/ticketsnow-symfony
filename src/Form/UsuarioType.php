<?php
/**
 * Created by PhpStorm.
 * User: irene
 * Date: 2/02/18
 * Time: 11:08
 */

namespace App\Form;


use App\Entity\Usuario;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UsuarioType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('username', TextType::class)
            ->add('apellidos', TextType::class)
            ->add('provincia', TextType::class)
            ->add('email', EmailType::class)
            ->add('password', PasswordType::class)
            ->add('password2', PasswordType::class, array('mapped' => false))
            ->add(
                'save',
                SubmitType::class,
                [
                    'label' => 'Guardar'
                ]);
    }
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Usuario::class,
        ));
    }
}