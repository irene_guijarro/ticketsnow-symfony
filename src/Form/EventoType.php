<?php

namespace App\Form;

use App\Entity\Categoria;
use App\Entity\Evento;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TimeType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EventoType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('titulo', TextType::class)
            ->add('descripcion', TextType::class)
            ->add('precio', MoneyType::class)
            ->add('imagen', FileType::class, ['label' => 'Imagen (PNG o JPG)'])
            ->add('categoria', EntityType::class, ['class' => Categoria::class])
            ->add('cantEntradasTotal', NumberType::class)
            ->add('fechaCelebracion', DateType::class)
            ->add('hora', TimeType::class)
            ->add('ciudad', TextType::class)
            ->add('lugar', TextType::class)
            ->add('fechaInicioVentaEntradas', DateType::class)
            ->add('fechaFinVentaEntradas', DateType::class)
            ->add('paginaWeb', TextType::class)
            ->add(
                'save',
                SubmitType::class,
                [
                    'label' => 'Guardar'
                ]);
    }
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Evento::class,
        ));
    }
}