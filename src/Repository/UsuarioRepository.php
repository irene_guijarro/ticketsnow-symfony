<?php
/**
 * Created by PhpStorm.
 * User: irene
 * Date: 5/02/18
 * Time: 16:46
 */

namespace App\Repository;


use App\Entity\Usuario;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

class UsuarioRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Usuario::class);
    }

    public function findUsuarios()
    {
        $qb = $this->createQueryBuilder('u');

        $qb->orderBy('u.username');

        return $qb->getQuery()->getResult();
    }

    public function findUsuarioById($id)
    {
        $qb = $this->createQueryBuilder('u');

        $qb->orderBy('u.username');

        return $qb->getQuery()->getResult();
    }

    public function getCountUsuarios()
    {
        $qb = $this->createQueryBuilder('u');

        $qb->select('count(u.id)');

        return $qb->getQuery()->getSingleScalarResult();
    }
}