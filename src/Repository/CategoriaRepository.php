<?php

namespace App\Repository;

use App\Entity\Categoria;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

class CategoriaRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Categoria::class);
    }

    public function findByNombre($busqueda)
    {
        $qb = $this->createQueryBuilder('c');

        $qb ->where($qb->expr()->like('c.nombre', ':busqueda'))
            ->setParameter('busqueda', '%' . $busqueda . '%');

        $qb->orderBy('c.nombre', 'ASC');

        return $qb->getQuery()->getResult();
    }
}