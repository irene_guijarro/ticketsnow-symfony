<?php
/**
 * Created by PhpStorm.
 * User: irene
 * Date: 18/02/18
 * Time: 15:20
 */

namespace App\Repository;


use App\Entity\Usuario_Compra_Evento;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

class UsuarioCompraEventoRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Usuario_Compra_Evento::class);
    }

    public function findEntradas()
    {
        $qb = $this->createQueryBuilder('r');

        $qb->orderBy('r.id');

        return $qb->getQuery()->getResult();
    }

    public function findEntradasUsuario($idUsuario)
    {
        $qb = $this->createQueryBuilder('e');

        $qb->innerJoin('e.idUsuario', 'usuario')
            ->where($qb->expr()->eq('usuario.id', ':id'))
            ->setParameter('id', $idUsuario);

        $parameters['id'] = $idUsuario;

        return $qb->getQuery()->getResult();
    }

    public function getCountRecibos()
    {
        $qb = $this->createQueryBuilder('r');

        $qb->select('count(r.id)');

        return $qb->getQuery()->getSingleScalarResult();
    }
}