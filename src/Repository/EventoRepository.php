<?php

namespace App\Repository;

use App\Entity\Evento;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Symfony\Bridge\Doctrine\RegistryInterface;

class EventoRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Evento::class);
    }

    public function findAllWithLimits($offset, $limit)
    {
        $qb = $this->createQueryBuilder('e')
            ->orderBy('e.fechaCelebracion')
            ->setMaxResults($limit);

        if (!is_null($offset))
            $qb->setFirstResult($offset);

        return $qb->getQuery()->getResult();
    }

    private function addBusqueda(QueryBuilder $qb, $busqueda = null, $busquedaCiudad = null, $busquedaFecha = null,
        $busquedaCategoria = null, $idUsuario = null)
    {
        $parameters = [];

        if((!is_null($idUsuario) && !empty($idUsuario)))
        {
                $qb->innerJoin('e.id_gestor', 'gestor');
                $qb->where($qb->expr()->eq('gestor.id', ':id'));

                $parameters['id'] = $idUsuario;
        }

        if((!is_null($busquedaCategoria) && !empty($busquedaCategoria)))
        {
            if ($busquedaCategoria !== 'all')
            {
                $qb->innerJoin('e.categoria', 'categoria');
                $qb->where($qb->expr()->eq('categoria.nombre', ':categoria'));

                $parameters['categoria'] = $busquedaCategoria;
            }
        }

        if ((!is_null($busqueda) && !empty($busqueda)) || (!is_null($busquedaCiudad) && !empty($busquedaCiudad)))
        {
            $qb->where($qb->expr()->like('e.titulo', ':busqueda'))
                ->andWhere($qb->expr()->like('e.ciudad', ':busquedaCiudad'));

            $parameters['busquedaCiudad'] = '%' . $busquedaCiudad . '%';
            $parameters['busqueda'] = '%' . $busqueda . '%';
        }

        if (!is_null($busquedaFecha))
        {
            switch ($busquedaFecha)
            {
                case 'all':
                    break;
                case 'tomorrow':
                {
                    $tomorrow = date("Y-m-d", strtotime('tomorrow'));
                    $qb ->andWhere('e.fechaCelebracion = :day');

                    $parameters['day'] = $tomorrow;
                }
                    break;
                case 'weekend':
                {
                    $diaSemanaHoy = date('l');

                    if ($diaSemanaHoy === 'Saturday')
                        $sabado = date('Y-m-d');
                    else
                        $sabado = date('Y-m-d', strtotime('next Saturday'));

                    if ($diaSemanaHoy === 'Sunday')
                        $domingo = date('Y-m-d');
                    else
                        $domingo = date('Y-m-d', strtotime('next Sunday'));

                    $qb ->andWhere('e.fechaCelebracion = :sabado')
                        ->orWhere('e.fechaCelebracion = :domingo');

                    $parameters['sabado'] = $sabado;
                    $parameters['domingo'] = $domingo;
                }
                    break;
                default:
                    break;
            }
        }

        if (count($parameters))
        {
            $qb->setParameters($parameters);
        }

    }

    public function findEventos($idUsuario, $busqueda = null, $busquedaCiudad = null, $busquedaFecha = null,
                                $busquedaCategoria = null, $offset = null, $limit = null)
    {
        $qb = $this->createQueryBuilder('e');

        $qb->orderBy('e.fechaCelebracion')
            ->setMaxResults($limit);

        $this->addBusqueda($qb, $busqueda, $busquedaCiudad, $busquedaFecha, $busquedaCategoria, $idUsuario);

        if (!is_null($offset))
            $qb->setFirstResult($offset);

        return $qb->getQuery()->getResult();
    }

    public function getEventosFiltrados($busqueda, $busquedaCiudad, $busquedaCategoria, $busquedaFecha, $order)
    {
        $qb = $this->createQueryBuilder('evento');

        if (isset($busqueda))
        {
            $qb->where($qb->expr()->like('evento.titulo', ':titulo'))
                ->setParameter('titulo', '%' . $busqueda . '%');
        }
        if (isset($busquedaCiudad))
        {
            $qb->where($qb->expr()->like('evento.ciudad', ':busquedaCiudad'))
                ->setParameter('busquedaCiudad', '%' . $busquedaCiudad . '%');
        }
        if (isset($busquedaCategoria))
        {
            $qb->innerJoin('evento.categoria', 'categoria');
            $qb->where($qb->expr()->eq('categoria.nombre', ':categoria'))
                ->setParameter('categoria', $busquedaCategoria);
        }
        if (isset($busquedaFecha))
        {
            switch ($busquedaFecha)
            {
                case 'all':
                    break;
                case 'tomorrow':
                {
                    $tomorrow = date("Y-m-d", strtotime('tomorrow'));
                    $qb ->where('evento.fechaCelebracion = :day')
                        ->setParameter('day', $tomorrow);
                }
                    break;
                case 'weekend':
                {
                    $diaSemanaHoy = date('l');

                    if ($diaSemanaHoy === 'Saturday')
                        $sabado = date('Y-m-d');
                    else
                        $sabado = date('Y-m-d', strtotime('next Saturday'));

                    if ($diaSemanaHoy === 'Sunday')
                        $domingo = date('Y-m-d');
                    else
                        $domingo = date('Y-m-d', strtotime('next Sunday'));

                    $qb ->where('evento.fechaCelebracion = :sabado')
                        ->orWhere('evento.fechaCelebracion = :domingo')
                        ->setParameters(['sabado' => $sabado, 'domingo' => $domingo]);
                }
                    break;
//                case 'week':
//                {
//                    $numeroSemana = date('W');
//
//                    $qb->where('WEEK(evento.fechaCelebracion) = :week')
//                        ->setPArameter('week', $numeroSemana);
//                }
//                    break;
//                case 'month':
//                {
//                    $numeroMes = date('M');
//
//                    $qb->where('MONTH(evento.fechaCelebracion) = :month')
//                        ->setPArameter('month', $numeroMes);
//                }
//                    break;
                default:
                    break;
            }
        }

        $qb->orderBy('evento.'.$order, 'ASC');

        return $qb->getQuery()->getResult();
    }

    public function getCountEventos($busqueda = null, $busquedaCiudad = null, $busquedacategoria = null, $busquedaFecha = null, $idUsuario = null)
    {
        $qb = $this->createQueryBuilder('e');

        $qb ->select('count(e.id)');

        $this->addBusqueda($qb, $busqueda, $busquedaCiudad, $busquedacategoria, $busquedaFecha, $idUsuario);

        return $qb->getQuery()->getSingleScalarResult();
    }
}