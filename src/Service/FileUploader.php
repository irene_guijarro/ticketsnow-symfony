<?php
namespace App\Service;

use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class FileUploader
{
    private $targetDir;

    public function __construct($targetDir)
    {
        $this->targetDir = $targetDir;
    }

    public function getImageName()
    {
        return md5 ( uniqid ());
    }
    public function upload(UploadedFile $file)
    {
        $fileName = $this->getImageName() . '.' . $file->guessExtension();

        $file->move($this->getTargetDir(), $fileName);

        return $fileName;
    }

    private function getExtensionB64Image($imagen)
    {
        $pos  = strpos($imagen, ';');
        $type = explode(':', substr($imagen, 0, $pos))[1];

        switch ($type)
        {
            case 'image/jpg':
            case 'image/jpeg':
                return 'jpg';

            case 'image/png':
                return 'png';

            case 'image/gif':
                return 'gif';

            default:
                throw new BadRequestHttpException('Formato de imagen no soportado');
        }
    }

    public function saveB64Imagen($imagenB64)
    {
        $arr_imagen = explode (',', $imagenB64);
        if ( count ($arr_imagen) < 2)
            throw new BadRequestHttpException('formato de imagen incorrecto');

        $imagen = base64_decode ($arr_imagen[1]);
        if (is_null ($imagen))
            throw new BadRequestHttpException('formato de imagen incorrecto');

        $fileName = $this->getImageName() . '.' . $this->getExtensionB64Image($imagenB64);
        $filePath = $this->getTargetDir() . '/' . $fileName;
        $ifp = fopen ($filePath, "wb");
        if (!$ifp)
            throw new BadRequestHttpException('Error al guardar el archivo de imagen');

        $ok = fwrite ($ifp, $imagen);
        if (!$ok)
            throw new BadRequestHttpException('Error al guardar el archivo de imagen');

        fclose ($ifp);

        return $fileName;
    }

    public function getTargetDir()
    {
        return $this->targetDir;
    }
}